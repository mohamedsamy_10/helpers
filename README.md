
# Laravel Helpers
Common helpers functions and shortcuts for laravel applications

## Installation

```php
composer require mosamy/helpers
```

```php
php artisan vendor:publish --provider="Mosamy\Helpers\HelpersServiceProvider" --tag="config"
```
## Usage

### Scope Trait
 This trait provide some model helpers.

```php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
  use \Mosamy\Helpers\Traits\Scopes;
}
```

<div>Then you can use the scopes.</div>

### whenWhere()

```php
$posts = Posts::whenWhere('type')->get();

//This excatly equivalent to:

$posts = Posts::when(request('type'), fn( $post) => $post->where('type', request('type')))->get();
```

```php
$posts = Posts::whenWhere('post_type', request('type')))->get();

//This excatly equivalent to:

$posts = Posts::when(request('type'), fn( $post) => $post->where('post_type', request('type')))->get();
```

```php
$posts = Posts::whenWhere('post_type', $condition, request('type'))->get();

//This excatly equivalent to:

$posts = Posts::when($condition, fn($post) => $post->where('post_type', request('type')))->get();
```


### whenLike()

```php
$posts = Posts::whenLike('type')->get();

//This excatly equivalent to:

$posts = Posts::when(request('type'), fn( $post) => $post->where('type', 'like', '%'.request('type').'%'))->get();
```

```php
$posts = Posts::whenLike('post_type', request('type'))->get();

//This excatly equivalent to:

$posts = Posts::when(request('type'), fn( $post) => $post->where('post_type', 'like', '%'.request('type').'%'))->get();
```


```php
$posts = Posts::whenLike('post_type', $condition, request('type')))->get();

//This excatly equivalent to:

$posts = Posts::when($condition, fn( $post) => $post->where('post_type', 'like', '%'.request('type').'%'))->get();
```
### whenNotLike()
The opposite of whenLike()

### whenWhereIn()

```php
$posts = Posts::whenWhereIn('type')->get();

//This excatly equivalent to:

$posts = Posts::when(request('type'), fn( $post) => $post->whereIn('type', request('type')))->get();
```

```php
$posts = Posts::whenWhere('post_type', request('type'))->get();

//This excatly equivalent to:

$posts = Posts::when(request('type'), fn( $post) => $post->whereIn('post_type', request('type')))->get();
```

```php
$posts = Posts::whenWhere('post_type', $condition, request('type'))->get();

//This excatly equivalent to:

$posts = Posts::when($condition, fn( $post) => $post->whereIn('post_type', request('type')))->get();
```
### whenWhereNotIn()
The opposite of whenWhereIn()

### whenWhereRelation()

```php
$posts = Posts::whenWhereRelation('category', 'name')->get();

//This excatly equivalent to:

$posts = Posts::when(request('name'), fn( $post) => $post->whereRelation('category', 'name', request('name')))->get();
```

```php
$posts = Posts::whenWhereRelation('category', 'category_name', request('name'))->get();

//This excatly equivalent to:

$posts = Posts::when(request('name'), fn( $post) => $post->whereRelation('category', 'category_name', request('name')))->get();
```

```php
$posts = Posts::whenWhereRelation('category', 'category_name', $condition, request('name'))->get();

//This excatly equivalent to:

$posts = Posts::when($condition, fn( $post) => $post->whereRelation('category', 'category_name', request('name')))->get();
```

### whenWhereRelationIn()

```php
$posts = Posts::whenWhereRelationIn('category', 'name')->get();

//This excatly equivalent to:

$posts = Posts::when(request('name'), fn( $post) =>
              $post->whereHas('category', fn($category) => $category->whereIn('name', request('name')))
         )->get();
```

```php
$posts = Posts::whenWhereRelationIn('category', 'category_name', request('name'))->get();

//This excatly equivalent to:

$posts = Posts::when(request('name'), fn( $post) =>
              $post->whereHas('category', fn($category) => $category->whereIn('category_name', request('name')))
         )->get();
```

```php
$posts = Posts::whenWhereRelationIn('category', 'category_name', $condition, request('name'))->get();

//This excatly equivalent to:

$posts = Posts::when( $condition, fn( $post) =>
              $post->whereHas('category', fn($category) => $category->whereIn('category_name', request('name')))
         )->get();
```


### findInSet()

```php
$posts = Posts::findInSet('tags', ['keyword1', 'keyword2', 'keyword3'])->get();
```

### From - To 

```php
// find between two dates, when datetime = true it will convert dates to datetime
$posts = Posts::fromTo($from, $to, $column, $datetime);

// find between two dates if ($to) is exists or whereDate $column = $from
$posts = Posts::scopeFromOrTo($from, $to, $column, $datetime);
```


### search()
```php
$posts = Posts::search($keyword, ['name', 'description', 'notes'])->get();

//This excatly equivalent to:

$posts = Posts::when($keyword, fn($post) =>
              $post->where(function($post){
                foreach (['name', 'description', 'notes'] as $attribute) {
                  $post->orWhere($attribute,'like','%'.$keyword.'%');
                }
              });
         )->get();
```

> You can set the default search attributes in your model.

```php

class Posts extends Model
{
  use \Mosamy\Helpers\Traits\Scopes;
  const SearchableAttributes = ['name', 'description', 'notes'];
}

$posts = Posts::search($keyword)->get();

```

### WithFilters Trait (Livewire 3)


> If you are using livewire, you may be use some filter with query string for search
> WithFilters Trait make it easy without making messy code!


```php

use App\Models\Product;
use Mosamy\Helpers\Traits\WithFilters;
use Livewire\Component;

class Products extends Component
{
    use WithFilters;
	
	// set your filters in $filter proberty as array and the value will consider the default filter value
	
	public $filters = [
		'search' => null,
		'category_id' => null,
		'sortby' => 'recent',
		'brands' => []
    ];
	
	public $products;
	
	public function mount()
    {
		
		$this->setFilter('sortby', 'low-price'); //set or change the default value of specific filter
		
	    $this->products = Product::whereStatus('active')
        ->where('category_id', $this->filter('category_id')) //get search value by using filter() function
        ->whereIn('brand_id', $this->filter('brands'))
		->get();
	}
	
}

```

```html
<input type="text" wire:model.live="filters.search">
```



> There is some rules and helper function you can look at thier files in
> the source code.

### WithValidator Trait

> Useful with api responses that return "Validation Error!" message with array of errors

### ComponentUtilities Trait

> Useful with livewire components with some functions
> redirectNotify: make redirect with notification
> navigate: navigate to page via SPA
> redirectWithSuccessTo: navigate to page via SPA with notification
> validateForm: validate form with scroll in error positio
> resetErrors: resetErrorBag & resetValidation
> rename: use to rename keys in a collection

### Resources

## Pagination:
> Useful for api resources that using paginaton to return a format include total, per_page and current_page
> simply you can use it as the following:
```php
return response()->json([
  'data' => UserResource::collection($data),
  'pagination' => new Pagination($data)
]);
```

## Resource:
> Useful when you need to use a single api resource file for both single model or collection
> simply you can use it as the following:
```php
return [
	'id' => $this->id,
	'name' => $this->name,
	$this->mergeWhen(self::$isCollection, [
	  'description' => $this->description // displays when the resource for model collection
	]),
	$this->mergeWhen(!self::$isCollection, [
	  'short_description' => $this->short_description // displays when the resource for single model only
	])
]
```






