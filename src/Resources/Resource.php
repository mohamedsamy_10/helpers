<?php

namespace Mosamy\Helpers\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Resource extends JsonResource
{

    public static $isCollection = false;
    
    public static function collection($resource)
    {
        self::$isCollection = true;
        return parent::collection($resource);
    }
}