<?php

namespace Mosamy\Helpers\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Pagination extends JsonResource
{
    public function toArray($request)
    {
      return [
          'total' => $this->total(),
          'per_page' => $this->perPage(),
          'current_page' => $this->currentPage()
      ];
    }
}
