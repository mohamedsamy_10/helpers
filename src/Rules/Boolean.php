<?php

namespace Mosamy\Helpers\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class Boolean implements ValidationRule
{
	public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if(!is_boolean($value)) $fail(':attribute must be a valid boolean');
    }
}
