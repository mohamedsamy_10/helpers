<?php

namespace Mosamy\Helpers\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class IsValidYoutube implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $valid = preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]{11})/", $value, $matches);
        if(!$valid) $fail('Youtube URL is invalid');
    }
}
