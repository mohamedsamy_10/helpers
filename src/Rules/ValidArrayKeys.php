<?php

namespace Mosamy\Helpers\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class ValidArrayKeys implements ValidationRule
{

    protected array $values = [];
    protected array $differents = [];

    public function __construct($values){
      $this->values = $values;
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $allowedKeys = array_flip($this->values);
        $this->differents = array_diff_key(array_keys($value), array_keys($allowedKeys));
        if(count($this->differents) !== 0)
        $fail(':attribute contains invalid fields ['.implode(',',$this->differents).']');
    }
}
