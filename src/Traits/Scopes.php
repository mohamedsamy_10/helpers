<?php

namespace Mosamy\Helpers\Traits;

trait Scopes {

    public function scopeWhenWhere($query, $whereColumn, $when = null, $whereValue = null){
        if($when === null) $when = request($whereColumn);
        if(!$whereValue) $whereValue = $when;
        return $query->when($when, fn($q) => $q->where($whereColumn, $whereValue));
    }

    public function scopeWhenLike($query, $whereColumn, $when = null, $whereValue = null){
        if($when === null) $when = request($whereColumn);
        if(!$whereValue) $whereValue = $when;
        return $query->when($when, fn($q) => $q->where($whereColumn, 'like', '%'.$whereValue.'%'));
    }

    public function scopeWhenNotLike($query, $whereColumn, $when = null, $whereValue = null){
        if($when === null) $when = request($whereColumn);
        if(!$whereValue) $whereValue = $when;
        return $query->when($when, fn($q) => $q->where($whereColumn, 'not like', '%'.$whereValue.'%'));
    }

    public function scopeWhenWhereIn($query, $whereColumn, $when = null, $whereValue = []){
        if($when === null) $when = request($whereColumn);
        if(!$whereValue) $whereValue = $when;
        return $query->when($when, fn($q) => $q->whereIn($whereColumn, $whereValue));
    }

    public function scopeWhenWhereNotIn($query, $whereColumn, $when = null, $whereValue = []){
        if($when === null) $when = request($whereColumn);
        if(!$whereValue) $whereValue = $when;
        return $query->when($when, fn($q) => $q->whereNotIn($whereColumn, $whereValue));
    }

    public function scopeWhenWhereRelation($query, $relation, $whereColumn, $when = null, $whereValue = null){
        if($when === null) $when = request($whereColumn);
        if(!$whereValue) $whereValue = $when;
        return $query->when($when, fn($q) => $q->whereRelation($relation, $whereColumn, $whereValue));
    }

    public function scopeWhenWhereRelationIn($query, $relation, $whereColumn, $when, $whereValue = []){
        if(!$whereValue) $whereValue = $when;
        return $query->when($when, function($query) use($relation, $whereColumn, $whereValue){
           $query->whereHas($relation, fn($q) => $q->whereIn($whereColumn, $whereValue));
        });
    }

    public function scopeSearch($query, $keyword, $attributes = []){
        if(!$attributes) $attributes = self::SearchableAttributes;
        return $query->when($keyword, fn($q) => $q->whereAny($attributes, 'like', '%'.$keyword.'%'));
    }
	
	public function scopeFindInSet($query, $field, $keywords)
    {
        if(!is_array($keywords)) $keywords = [$keywords];
        $query->when($keywords, function ($query) use ($field, $keywords) {
            $query->where(function ($query) use ($field, $keywords) {
                foreach($keywords as $keyword) {
                    $query->orWhereRaw('FIND_IN_SET("'.$keyword.'", '.$field.')');
                }
            });
        });
    }

    // from to filter

    public function scopeFromTo($query, $from = null, $to = null, $column = 'created_at', $datetime = false)
    {
		if($from && $to && $datetime){
			$from = now()->parse($from)->startOfDay();
			$to = now()->parse($to)->endOfDay();
		}
		
        return $query->when($from && $to, fn($q) => $q->whereBetween($column, [$from, $to]));
    }

    public function scopeFromOrTo($query, $from = null, $to = null, $column = 'created_at', $datetime = false)
    {
		if($datetime){
			if($from) $from = now()->parse($from)->startOfDay();
			if($to) $to = now()->parse($to)->endOfDay();
		}
		
        return $query
		->when($from && $to, fn($q) => $q->fromTo($from, $to, $column, $datetime))
		->when($from && !$to, fn($q) => $q->whereDate($column, $from));
    }
	

}

 ?>
