<?php

namespace Mosamy\Helpers\Traits;

trait WithFilters
{
    private $filterQueryString = [];

    public function __construct()
    {
        $this->filters ??= ['search' => ''];
        foreach($this->filters as $key => $value) {
            $this->filterQueryString['filters.'.$key] = ['except' => '', 'as' => $key];
        }
    }

    protected function queryStringWithFilters()
    {
        return $this->filterQueryString;
    }

    public function setFilter($key, $value)
    {
        $this->filters[$key] = $value;
    }

    public function filter($key)
    {
        return $this->filters[$key] ?? null;
    }

    public function updatingFilters()
    {
        if(method_exists($this, 'resetPage'))
        $this->resetPage();
    }

}
