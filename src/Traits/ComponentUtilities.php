<?php

namespace Mosamy\Helpers\Traits;

use Livewire\Attributes\On;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

trait ComponentUtilities
{

    #[On('redirectNotify')]
    public function redirectNotify(string $url, array $notification)
    {
        redirect($url)->notify($notification['title'] ?? null, $notification['message'], $notification['status'] ?? 'success');
    }

    #[On('navigate')]
    public function navigate(string $url)
    {
        $this->redirect($url, navigate:true);
    }

    public function redirectWithSuccessTo($to)
    {
        $this->dispatch('notify', message: trans('app.saved'));
        if($to != request()->headers->get('referer'))
        $this->dispatch('navigate', $to);
    }

    public function validateForm($form = null)
    {
        $fn = $form ? fn () => $form->validate() : fn() => $this->validate();
        $this->resetErrors();
        $this->scrollOnFail($fn);
    }

    public function resetErrors()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function deleteMedia($mediaID, $wireModal)
    {
        if($mediaID){
            $media = Media::find($mediaID);
            $modelClass = $media->model_type;
            $modelId = $media->model_id;
    
            $media->delete();
    
            foreach (get_object_vars($this) as $property => $value) {
                if ($value instanceof $modelClass && $value->id == $modelId) {
                    $this->{$property}->refresh();
                }
            }
        } else {
            $this->reset($wireModal);
        }
    }

}
