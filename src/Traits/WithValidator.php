<?php

namespace Mosamy\Helpers\Traits;

trait WithValidator
{
    public function withValidator($validator){
      if($validator->fails())
      throwFailure('Validation Error!',$validator->errors());
    }
}

?>
