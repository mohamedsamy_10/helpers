<?php

return [
  'pagination' => 60,
  'status' => ['active', 'inactive'],
  'languages' => ['en','ar'],
  'file_extentions_allowed' => [
    'images' => ['jpg','jpeg','gif','png'],
    'docs' => ['pdf','doc','docx','xlsx','xls','csv','txt','pptx','ppt'],
    'compressed' => ['zip','zipx','tz','7z','tar','rar'],
    'audios' => ['mp3','mpeg','mpga'],
    'videos' => ['mp4']
  ],
  /*'guards' => [
    'admin' => [
      'title' => 'Admin Dashboard',
      'prefix' => 'webadmin'
    ],
    'user' => [
      'title' => null,
      'prefix' => null
    ]
  ]*/

];

?>
