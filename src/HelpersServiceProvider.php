<?php

namespace Mosamy\Helpers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Routing\Redirector;

class HelpersServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {


      $this->mergeConfigFrom(__DIR__.'/Config.php', 'settings');

      Request::macro('validatedExcept', function ($except = []) {
      	return Arr::except($this->validated(), $except);
      });

      Request::macro('validatedExceptWith', function ($except = [], $with = []) {
      	return array_merge(Arr::except($this->validated(), $except), $this->only($with));
      });

      Str::macro('snakeToTitle', function($value) {
        return str()->title(str_replace('_', ' ', $value));
      });

      Collection::macro('whenWhere', function ($whereColumn, $when = null, $whereValue = null) {
          if(!$when) $when = request($whereColumn);
          if(!$whereValue) $whereValue = $when;
          return $this->when($when, fn($q) => $q->where($whereColumn, $whereValue));
      });

      Collection::macro('sortKeysAs', function($order){
        return $this->sortBy( fn($item, $key) => array_search($key, $order));
      });

		Collection::macro('rename', function (array $keys) {
			// Handle arrays of associative arrays
			if (is_array($this->first())) {
				return $this->map(function ($item) use ($keys) {
					foreach ($keys as $oldKey => $newKey) {
						if (array_key_exists($oldKey, $item)) {
							$item[$newKey] = $item[$oldKey];
							unset($item[$oldKey]);
						}
					}
					return $item;
				});
			} else {
				// Handle single associative array
				$this->map(function ($value, $key) use ($keys) {
					if (array_key_exists($key, $keys)) {
						$this->items[$keys[$key]] = $value;
						unset($this->items[$key]);
					}
					return $value;
				});
			}

			return $this;
		});
		
		

      Redirector::macro('notify', function ($title, $message, $type = 'success') {
          session()->flash('notify', [
            'type' => $type,
            'title' => $title,
            'message' => $message
        ]);
      });

    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
      //php artisan vendor:publish --provider="Mosamy\Helpers\HelpersServiceProvider" --tag="config"
      if ($this->app->runningInConsole()) {
        $this->publishes([
          __DIR__.'/Config.php' => config_path('settings.php'),
        ], 'config');
      }
    }
}
