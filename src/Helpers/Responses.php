<?php

  function validateRequest($request,$data){
      $validator = Validator::make($request->all(),$data);
      if($validator->fails())
      throwFailure('Validation Error!',$validator->errors());
  }

  function throwSuccess($data = [], $message = 'Data have been processed successfully', $statusCode = 200){
      response(array_filter([
          'status' => 'success',
          'response' => $data,
          'message' => $message,
      ]), $statusCode, [], JSON_NUMERIC_CHECK)->throwResponse();
  }

  function throwFailure($message = '', $errors = [], $statusCode = 422){
      response(array_filter([
          'status' => 'failure',
          'errors' => $errors,
          'message' => $message,
      ]), $statusCode, [], JSON_NUMERIC_CHECK)->throwResponse();
  }

  function withPagination($query, $resource = null){
    $data = $resource ? ('App\Http\Resources\\'.$resource)::collection($query) : $query->items();
    return [
      'data' => $data,
      'pagination'=> new App\Http\Resources\Pagination($query)
    ];
  }

  function withTranslations($query){
    if($query instanceof \Illuminate\Support\Collection || $query instanceof \Illuminate\Pagination\LengthAwarePaginator){
      return $query->append('translations_list')
      ->map(function($item){
        $item = collect($item);
        $item['translations'] = $item['translations_list'];
        return $item->except(['translations_list']);
      });
    } else {
      $query = collect($query->append('translations_list'));
      $query['translations'] = $query['translations_list'];
      return $query->except(['translations_list']);
    }
  }

  function withPaginateTranslation($query, $resource = null){
    $data = $resource ? ('App\Http\Resources\\'.$resource)::collection($query) : $query;
    return [
      'data' => withTranslations($data),
      'pagination'=> new App\Http\Resources\Pagination($query)
    ];
  }

?>
