<?php
  function number_sign($num){
    return sprintf("%+d",$num);
  }

  function is_boolean($value){
    return filter_var(in_array($value, ["true", "false", true, false, "1", "0", 1, 0], true), FILTER_VALIDATE_BOOLEAN);
  }

  function is_true($value){
    return filter_var($value, FILTER_VALIDATE_BOOLEAN);
  }

  function multi_array_search($array, $keyword){
    foreach ($array as $key => $value) {
      if(array_search($keyword, $value)) return $key;
    }
  }

  function slug($string, $separator = '-') {
      $string = trim($string);
      $string = mb_strtolower($string, "UTF-8");
      $string = preg_replace("/[^a-z0-9_\sءاأإآؤئبتثجحخدذرزسشصضطظعغفقكلمنهويةى]#u/", "", $string);
      $string = preg_replace("/[\s-]+/", " ", $string);
      $string = preg_replace("/[\s_]/", $separator, $string);
      return $string;
  }

  function clean_content($string) {
     $string = strip_tags($string);
     $string = str_replace('&nbsp;',' ', $string);
     return $string;
  }

  function url_with_params($path, $params = []) {
    $url = url($path);
    if($params) $url .= '?' . http_build_query($params);
    return $url;
  }

  function url_by_guard($segment = null, $params = [], $guard = null){
      if(!$guard) $guard = auth()->getDefaultDriver();
      return url_with_params(config('settings.guards.'.$guard.'.prefix').'/'.$segment, $params);
  }


  function lastSegment($index = 0) {
     return collect(request()->segments())->reverse()->values()->get($index);
  }

  function youtube($link, $return = 'url'){
    if($link){
      $parsedUrl = parse_url($link,PHP_URL_QUERY);
      parse_str($parsedUrl,$param);
      if(isset($param['v'])){
        return $return=='url' ? 'https://www.youtube.com/embed/'.$param['v'] : 'http://i3.ytimg.com/vi/'.$param['v'].'/sddefault.jpg';
      }
    }
    return false;
  }

?>
